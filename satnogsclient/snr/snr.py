import sys
sys.path.append("/var/lib/satnogs/lib/python3.9/site-packages/satnogsclient/snr/")

import logging
import threading
from computesnr import MEASUREDATSNR

LOGGER = logging.getLogger(__name__)

class SNR():
    """Class for running SNR code."""

    # sleep time of loop (in seconds)
    _sleep_time = 0.1

    # loop flag
    _stay_alive = False

    # end when this timestamp is reached
    _observation_end = None

    def __init__(self, observation_id, time_to_stop=None, sleep_time=None):
        """Initialize snr class."""
        if time_to_stop:
            self._observation_end = time_to_stop
        if sleep_time:
            self._sleep_time = sleep_time
        self.observation_id = observation_id
        # Low pass = 220 because of satnogs low pass filter (LPF) settings at the time of writing. Need to make robust
        # Satnogs LPF samp_rate = 57.6k, or bandwidth = 28.8kHz. Cutoff_freq = 12k.
        # Above means everything otuside bandwidth of 16.8kHz is LPF'd.
        # With bin size of 1024, this means low_pass argument needs to be (12/28.8)*512 = 213.3. Round up to 220
        # There's no DSP equation used to get above value, so don't think too hard about it
        # low_pass = 220 still sampled in the LPF'd range. Haven't debugged yet. Adding 100 to make sure we are in signal range.
        self.computesnr = MEASUREDATSNR("snr_"+str(observation_id)+".txt")

    @property
    def is_alive(self):
        """Returns if snr is alive or not."""
        return self._stay_alive

    @is_alive.setter
    def is_alive(self, value):
        """Sets value if tracking loop is alive or not."""
        self._stay_alive = value

    def snrstart(self):
        """
        Starts the thread that starts SNR program
        Stops by calling trackstop()
        """
        self.is_alive = True
        LOGGER.info('SNR initiated')
        if not all([self.observation_id]):
            e = ValueError('Observation id is not defined.')
            LOGGER.error(e)
            raise e

        self.snr = threading.Thread(target=self.computesnr.main)
        self.snr.daemon = True
        self.snr.start()
        return self.is_alive

    def snrstop(self):
        """
        Sets object flag to false and stops the tracking thread.
        """
        self.computesnr.stop()
        self.is_alive = False
        self.snr.join()
        LOGGER.info('SNR stopped.')
