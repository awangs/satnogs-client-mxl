"""
Developed by Kimberly Zwolshen (kxzwolsh) for the Michigan eXploration Laboratory
Reads in data through a ZMQ TCP address, and roughly calculates the SNR live
Flags can be used to customize the program and its output

Note for GNURadio flowgraph-
Use a ZMQ PUB Sink with the following inputs:
    IO Type - Complex
    Vector Length - 1024 (or whatever size FFT you are using)
    Address - tcp://*:55555 (localhost since you're probably running this program locally)

I don't really know what the other options are, but as long as the above are fine, it
should work alright

Edited by TAN and AW for testing with low-cost GS. Thanks Kim :)
"""
import zmq
import numpy as np
import time
import datetime
import logging

LOGGER = logging.getLogger(__name__)

class MEASUREDATSNR():
    def __init__(self, filename, numFFTs = 100, signalBandwidth = 30, center_frequency = 512):
        # Frequency deviation for FSK is +-3kHz. Sample rate out of satnogs' LPF is 57.6kHz, or +-28.8kHz
        # At 1024 bins, the signal should be 512 * (3/28.8) = +- 53 bins around the center.
        # Defaulting bins argument to 30 just in case, since above has not been tested.
        self.runTime = numFFTs
        self.bins = signalBandwidth
        self.center_frequency = center_frequency
        self.filename = filename
        self.run = True
        self.noiseFloor = []
        LOGGER.info('SNR filename: /home/pi/SNRdata/%s', self.filename)

    def arrayAvg(self, plusMin, index, arr):
            array = []
            for z in range(plusMin):
                array.append(arr[index-z])
                array.append(arr[index+z])
            return np.mean(array)

    def stop(self):
        self.run = False
 
    def main(self):
        LOGGER.info("Program started at " + str(datetime.datetime.now()))
        #---------------------------------------- socket ----------------------------------------#
        # Socket for the signal and SNR calculator
        context = zmq.Context()
        socket = context.socket(zmq.SUB)
        socket.connect("tcp://100.71.212.55:12001")
        socket.setsockopt(zmq.SUBSCRIBE, b'')
        
        LOGGER.info("Listening for client...")
        #------------------------------------- actual funcs -------------------------------------#
        runTimeCount = 0
        avgNoiseMatrix = []
        #_____________________________________ measure noise floor ____________________
        # theres sorry lots of repeated code
        while self.run and (runTimeCount < self.runTime):
            #This happens for every single broadcasted signal
            if(socket.poll(1000) != 0):  #1000 is the time to wait in ms I think
                    #  Receive next request from client
                message = socket.recv()
                data = np.frombuffer(message, dtype=np.complex64, count = -1)
                packet = []
                for x in range(1024):
                    packet.append(data[x])

                # Make some empty arrays
                mag = []
                db = []

                # Calculate magnitude then decibels, mag not important
                for x in range(len(packet)):
                        # this is not really magnitude anymore, but a lot of calculations
                            # performed after converting to magnitude
                    mag.append((np.abs(packet[x])/1024)**2) # compute power spectrum
                    #db.append(np.abs(packet[x]))

                    if(mag[x] == 0):
                        continue
                    else:
                        db.append(10*np.log10(mag[x]))

                # Recording noise floor over target range into a big 2D array
                avgNoiseMatrix.append(db[(self.center_frequency-self.bins) : (self.center_frequency+self.bins)])
                runTimeCount += 1
            else:
                time.sleep(0.1)
        self.noiseFloor = np.array(avgNoiseMatrix).mean(axis=0)

        #__________________________________ measure snr __________________________________
        while self.run:
            #This happens for every single broadcasted signal
            if(socket.poll(1000) != 0):  #1000 is the time to wait in ms I think
                #  Receive next request from client
                message = socket.recv()
                data = np.frombuffer(message, dtype=np.complex64, count = -1)
                packet = []

                for x in range(1024):
                    packet.append(data[x])
                    
                # Make some empty arrays
                mag = []
                db = []

                # Calculate magnitude then decibels, mag not important
                for x in range(len(packet)):
                    # this is not really magnitude anymore, but a lot of calculations
                        # performed after converting to magnitude
                    mag.append((np.abs(packet[x])/1024)**2) # compute power spectrum
                    #db.append(np.abs(packet[x]))
                    if(mag[x] == 0):
                        continue
                    else:
                        db.append(10*np.log10(mag[x])) 
                
                # Recording signal strengths at targeted bins
                signal = db[(self.center_frequency-self.bins) : (self.center_frequency+self.bins)]
                signal = np.array(signal)

                # Calculating the SNR. Subtracting signal strength array by noise array, so we get a SNR array
                snrArray = np.subtract(signal, self.noiseFloor)

                # Mean of array will get us mean SNR
                snr = np.mean(snrArray)
                with open("/home/pi/SNRdata/"+str(self.filename), 'a') as f:
                    f.write(f"{time.time()}, {snr}\n")
            # Waits a little bit before checking if there's a signal again (.1 sec)
            else:
                time.sleep(0.1)

        else:
            LOGGER.info("Ending program...")
            time.sleep(1)
            LOGGER.info("Program ended at " + str(datetime.datetime.now()))
